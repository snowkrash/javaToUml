import re

from antlr4 import *
import math
import sys
import os

sys.path.insert(1, 'lib')

from JavaLexer import JavaLexer
from JavaParser import JavaParser
from JavaParserListener import JavaParserListener
from JavaParserVisitor import JavaParserVisitor
from antlr4.StdinStream import StdinStream
from antlr4.CommonTokenStream import CommonTokenStream
from antlr4.tree.Tree import ParseTreeWalker

FONTSIZE = 15
PADD = 5
STARTPOINT = (0, 0)
not_id = ["class", "public", "static",
          "private", 'protected', "final", "abstract", "transient",'abstract',
'assert',
'boolean',
'break',
'byte',
'case',
'catch',
'char',
'class',
'const',
'continue',
'default',
'do',
'double',
'else',
'enum',
'extends',
'final',
'finally',
'float',
'for',
'if',
'goto',
'implements',
'import',
'instanceof',
'int',
'interface',
'long',
'native',
'new',
'package',
'private',
'protected',
'public',
'return',
'short',
'static',
'strictfp',
'super',
'switch',
'synchronized',
'this',
'throw',
'throws',
'transient',
'try',
'void',
'volatile',
'while',
]#extracted from JavaLexer.g4 avec head, tail and sed -E "s/.*'(.*)';/'\1',/g"


# TODO add comments, add packages and directories gestions,
# rename some variables, make antlr install fonction/script

class JavaSigGetter(JavaParserListener):
    def __init__(self):
        super()
        self.sig = []

    def enterClassDeclaration(self, ctx):
        classSig = getMethodsSymbols(ctx)
        classSig = re.sub("=.*;", ";", classSig)
        classSig = classSig.replace(" ( ", "(").replace(" )", ")")\
            .replace(" ;", ";").replace("\n\n", "\n").replace(" ]", "]")\
            .replace(" [", '[').replace(" < ", "<").replace(" > ", ">")  # prettyfy
        self.sig.append(classSig)

    def getSig(self):
        return self.sig


def getMethodsSymbols(ctx):
    string = ""
    if ctx.__class__ is JavaParser.BlockContext:
        return ';\n'
    elif ctx.__class__ is tree.Tree.TerminalNodeImpl:
        return ctx.getSymbol().text+" "
    elif ctx.__class__ is JavaParser.AnnotationContext:  # don't deal with annotations for now
        return ""
    else:
        if ctx.__class__ is JavaParser.ClassBodyDeclarationContext:
            string = "\n"
        for i in ctx.getChildren():
            # ~ print(i.__class__)
            string += getMethodsSymbols(i)
        return string


def sigToUML(classSig: str):
    lines = classSig.split("\n")
    attributs = [
        li for li in lines if '(' not in li and ';' in li and li.strip(" ") != ';']
    meths = [li for li in lines if '(' in li and ';' in li]
    umlmeths = [util_to_UML(m) for m in meths]
    umlattributs = [util_to_UML(a) for a in attributs]
    return umlattributs, umlmeths


def umlToSVG(class_name: str, umlattributs: list, umlmeths: list, startpoint: tuple, fontsize: float = FONTSIZE, padd: float = PADD):
    uml = ""
    max_width = max([len(line) for line in umlattributs+umlmeths+[class_name]])
    width = (max_width*fontsize)/2
    heigth = (len(umlmeths+umlattributs)+1)*(fontsize+padd)+padd
    # big rect
    uml += util_get_rect(startpoint, width, heigth)
    # add classe_name
    uml += util_get_rect(startpoint, width, fontsize+padd)
    uml += util_get_text((startpoint[0]+padd,
                          startpoint[1]), fontsize, class_name)
    # add attributs
    pointAtt = (startpoint[0], startpoint[1]+fontsize+padd)
    uml += util_get_rect(pointAtt, width,
                         (fontsize+padd)*len(umlattributs))
    for a in umlattributs:
        uml += util_get_text((pointAtt[0]+padd, pointAtt[1] + (fontsize+padd)*umlattributs.index(a)),
                             fontsize,
                             a.replace("<", "&lt;").replace(">", "&gt;")
                             )
    pointmeths = (pointAtt[0], pointAtt[1] +
                  len(umlattributs)*(fontsize+padd))
    for m in umlmeths:
        uml += util_get_text(
            (pointmeths[0]+padd, pointmeths[1] +
             (fontsize+padd)*umlmeths.index(m)),
            fontsize,
            m.replace("<", "&lt;").replace(">", "&gt;")
        )
        if "):" not in m:
            uml += util_get_underline(
                (pointmeths[0]+padd+fontsize/2, pointmeths[1] +
                 (fontsize+padd)*umlmeths.index(m)+fontsize),
                (len(m)-2)*fontsize/2
            )
    return uml


def getClassName(sig: str):
    return [w for w in sig.split("\n")[0].split(" ") if w not in not_id][0].replace('<','&lt;').replace('>','&gt;')


def util_parse_visibility(line: str):
    line = " "+line
    if " public " in line:
        return '+'
    if " private " in line:
        return '-'
    return ''


def util_get_name_and_arg(line: str):
    line = re.sub(r'>([^>]*)', r'> \1', line)
    try:
        full = re.findall(r'[^ ]*\([^)]*\)', line)[0]
        # ~ print(full)
        name = re.findall(r'^[^ \(]*\(', full)[0][:-1]
    except IndexError:  # no () in line
        return [x for x in line.split(' ') if x != "" and x not in not_id][-1][:-1]
    args = full[len(name)+1:-1].replace(", ", ",")
    formatedargs = ""
    for stuff in args.split(','):
        if stuff in " ":
            continue
        # ~ print("stuff=%s"%stuff,"_")
        ws = [x for x in stuff.split(" ") if x not in " "]
        nameArg = ws[-1]
        formatedargs += nameArg+":"+" ".join(ws[:-1])+","
    if formatedargs != "":
        formatedargs = formatedargs[:-1]  # removaing trailing ,
    return name+'('+formatedargs+')'


def util_get_rect(startpoint: tuple, width: float, heigth: float):
    return '<rect x="%s" y="%s" width="%s" height="%s" stroke-width="1" fill-opacity="0" stroke="black" />' % (
        startpoint[0], startpoint[1], width, heigth)


def util_get_text(startpoint: tuple, fontsize: int, txt: str):
    return '<text x="%s" y="%s" fontsize="%s" font-family="monospace">%s</text>' % (
        startpoint[0], startpoint[1]+fontsize, fontsize, txt)


def util_get_underline(point: tuple, length: float):
    return '<line x1="%s" y1="%s" x2="%s"  y2="%s" stroke="black" stroke-width="1"/>' % (
        point[0], point[1], point[0]+length, point[1]
    )


def util_get_type(line: str):
    while re.match(r'.*<[^>]* [^<]*>.*', line):
        line = re.sub(r'(<[^ ]*) ([^>]*>)', r'\1\2', line)
    line = re.sub(r'>([^>])', r'> \1', line)
    ret_type = [w for w in line.split(" ") if w not in not_id][0]
    if "(" in ret_type:
        return ''
    # ~ if ret_type=='void':return ''
    return ':'+ret_type.replace('<','&lt;').replace('>','&gt;')


def util_to_UML(line: str):
    return util_parse_visibility(line) +\
        util_get_name_and_arg(line) +\
        util_get_type(line)

def getLinkedTo(sig: str):
    rep = []
    after = False
    line = sig.split('\n')[0]
    if " extends " not in line and " implements " not in line:
        return []
    for w in line.split(" "):
        w = w.strip(",")
        if w == "extends" or w == "implements":
            after = True
            continue
        if after and w not in ',{':
            rep.append(w)
    return rep


def get_files(directory):
    files = set()
    for dirname, dirnames, filenames in os.walk(directory):
        if '.git' in dirnames:
            dirnames.remove('.git')
        for subdirname in dirnames:
            files.union(get_files(os.path.join(directory, subdirname)))
        for filename in filenames:
            if filename.endswith(".java"):
                files.add(os.path.join(dirname, filename))
    return files


def getSig(stream):
    lexer = JavaLexer(stream)
    stream = CommonTokenStream(lexer)
    parser = JavaParser(stream)
    tree = parser.compilationUnit()
    printer = JavaSigGetter()
    walker = ParseTreeWalker()
    walker.walk(printer, tree)
    sig = printer.getSig()
    return sig


class UMLClass():
    name = ""
    methods = []
    attributs = []
    linked = []
    startpoint = (0, 0)
    package = ""
    lastancre = 0

    def __init__(self, classSig):
        self.name = getClassName(classSig)
        self.attributs, self.methods = sigToUML(classSig)
        self.linked = getLinkedTo(classSig)

    def toStr(self):
        return umlToSVG(self.name, self.attributs, self.methods, self.startpoint)

    def get_width(self):
        max_width = max([len(line)
                         for line in self.attributs+self.methods+[self.name]])
        width = (max_width*FONTSIZE)/2
        return width

    def get_heigth(self):
        heigth = (len(self.methods+self.attributs)+1)*(FONTSIZE+PADD)+PADD
        return heigth

    def get_where_to_link_child(self, nb):
        pt = (self.startpoint[0] + self.get_width()/nb*(self.lastancre %
                                                        nb+0.5), self.startpoint[1]+self.get_heigth())
        self.lastancre += 1
        return pt


class UMLDiagram():
    classes = []
    classesPadding = 50
    hierarchy = dict()
    max_rigth = 0
    max_heigth = 0

    def addClass(self, classe: UMLClass):
        self.classes.append(classe)

    def after_all_add(self):
        for c in self.classes:
            self.remove_parents_methodes(c)
        for c in self.classes:
            self.hierarchy[c] = self.get_hierachy_of(c)
        classes = sorted(self.classes, key=lambda c: c.name)
        # TODO group in a clever way children of multiples parents in classes
        # TODO represents attributs in classes with arrows
        # set startpoint in UMLClasses
        tokenLeftSpace = 0
        heigths = self.get_heigths()
        for c in classes:
            if self.hierarchy[c] != 0:
                continue
            self.putChildren(tokenLeftSpace, c, heigths)
            tokenLeftSpace += self.get_all_children_width(c)
        self.max_rigth = max([x.startpoint[0]+x.get_width()
                              for x in self.classes])+self.classesPadding
        self.max_heigth = max(heigths.values()) +\
            max([c.get_heigth() for c in classes if self.hierarchy[c]
                 == max(self.hierarchy.values())])+self.classesPadding
        self.simplify_parents()

    def putChildren(self, tokenLeftSpace, c, heigths):
        width = self.get_all_children_width(c)
        c.startpoint = (tokenLeftSpace+width/2-c.get_width()/2,
                        heigths[self.hierarchy[c]]+self.classesPadding,)
        for child in self.classes:
            if c.name in child.linked:
                tokenLeftSpace = self.putChildren(
                    tokenLeftSpace, child, heigths)
        return tokenLeftSpace+width

    def simplify_parents(self):
        for k in self.classes:  # on repete len(classes) fois
            for c in self.classes:
                for p in c.linked:
                    p = [x for x in self.classes if x.name == p]
                    if p == []:
                        continue
                    p = p[0]
                    for pl in p.linked:
                        if pl in c.linked:
                            c.linked.remove(pl)

    def get_heigths(self):
        res = dict()
        res[0] = 0
        for i in self.hierarchy.values():
            res[i+1] = max([c.get_heigth()
                            for c in self.classes if self.hierarchy[c] == i])+self.classesPadding
        return res

    def get_hierachy_of(self, classe):
        if(classe in self.hierarchy.keys()):
            return self.hierarchy[classe]
        parents = [p for p in self.classes if p.name in classe.linked]
        if parents == []:
            return 0
        return 1+max([self.get_hierachy_of(p) for p in parents])

    def get_all_children_width(self, c: UMLClass):
        children = [cl for cl in self.classes if c.name in cl.linked]
        return max(c.get_width()+self.classesPadding, sum([self.get_all_children_width(cl) for cl in children]))

    def remove_parents_methodes(self, classe: UMLClass):
        for parent in classe.linked:
            p = [c for c in self.classes if c.name == parent]  # get parent
            if len(p) == 0:
                continue
            # list of herited methods
            r = [m for m in p[0].methods if m in classe.methods]
            for m in r:
                classe.methods.remove(m)

    def put_arrows(self, c, p):
        size = 25
        txt = ""
        start = (c.startpoint[0]+c.get_width()/2, c.startpoint[1])
        end = p.get_where_to_link_child(
            len([x for x in self.classes if p.name in x.linked]))
        txt += '<line x1="%s" y1="%s" x2="%s"  y2="%s" stroke="black" stroke-width="1"/>' % (
            start[0], start[1], end[0], end[1]
        )
        txt += '<polyline points="%s %s, %s %s, %s %s,%s %s" stroke="black" stroke-width="1" fill="white" transform="rotate(%s,%s,%s)" />' % (
            end[0], end[1], end[0]+size, end[1]-size/2, end[0]+size, end[1]+size/2, end[0], end[1], -
            math.atan((end[0]-start[0])/(end[1]-start[1])) *
            180/math.pi+90, end[0], end[1]
        )
        return txt

    def __str__(self):
        self.after_all_add()
        txt = '<svg viewBox="0 0 %s %s">' % (self.max_rigth, self.max_heigth)
        for c in self.classes:
            txt += c.toStr()
            for p in c.linked:
                p = [x for x in self.classes if x.name == p]
                if p == []:
                    continue
                p = p[0]
                txt += self.put_arrows(c, p)
        return txt+"</svg>"


if __name__ == '__main__':
    sig = []
    if len(sys.argv) < 1 or "-help" in sys.argv:
        print(
            "usage: %s directoryname|- [outputfile.svg] " % sys.argv[0], file=sys.stderr)
        exit(1)
    if sys.argv[1] == '-' in sys.argv:
        sig = getSig(StdinStream(encoding="utf-8"))
    else:
        for filename in get_files(sys.argv[1]):
            sig += getSig(FileStream(filename,encoding="utf-8"))
            # ~ print(sig)
    # ~ print(sig)
    Diag = UMLDiagram()
    for classSig in sig:
        Diag.addClass(UMLClass(classSig))
    if len(sys.argv) > 2:
        try:
            of = open(sys.argv[2], 'w')
            of.write(str(Diag))
            of.close()
        except IOError as e:
            raise IOError("couldn't open/write output file")
    else:
        print(Diag)
