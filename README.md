# JavaToUml

tool(s) to extract uml classes from java source files into an SVG document

# Usage:

python3 java2uml.py /directory/to/javaFiles/ [outputFile.svg]  
cat file.java | python3 java2uml.py - [outputFile.svg]

# Requirement/Install

- python3
- antlr4 (pip install)

# [optional] Build of lexer and parser files using JavaLexer.g4 and JavaParser.g4

`cd lib/`  
`java -Xmx500M -cp "/path/to/lib/antlr-4.8-complete.jar:$CLASSPATH" org.antlr.v4.Tool -Dlanguage=Python3 Java*.g4`

require antlr, antlr4-python3-runtime (pip install), lib/Java*.g4 (https://github.com/antlr/grammars-v4)

(generates everything in lib, g4 files are under OpenBSD license)
